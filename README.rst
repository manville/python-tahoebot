========
Overview
========

.. start-badges

.. list-table::
    :stub-columns: 1

    * - docs
      - |docs|
    * - tests
      - |
        |
    * - package
      - | |version| |wheel| |supported-versions| |supported-implementations|
        | |commits-since|
.. |docs| image:: https://readthedocs.org/projects/python-tahoebot/badge/?style=flat
    :target: https://readthedocs.org/projects/python-tahoebot
    :alt: Documentation Status

.. |version| image:: https://img.shields.io/pypi/v/tahoebot.svg
    :alt: PyPI Package latest release
    :target: https://pypi.org/project/tahoebot

.. |commits-since| image:: https://img.shields.io/github/commits-since/nsmanville/python-tahoebot/v0.0.0.svg
    :alt: Commits since latest release
    :target: https://github.com/nsmanville/python-tahoebot/compare/v0.0.0...master

.. |wheel| image:: https://img.shields.io/pypi/wheel/tahoebot.svg
    :alt: PyPI Wheel
    :target: https://pypi.org/project/tahoebot

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/tahoebot.svg
    :alt: Supported versions
    :target: https://pypi.org/project/tahoebot

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/tahoebot.svg
    :alt: Supported implementations
    :target: https://pypi.org/project/tahoebot


.. end-badges

A chatbot used to reserve the Tahoe house.

* Free software: BSD 2-Clause License

Installation
============

::

    pip install tahoebot

Documentation
=============


https://python-tahoebot.readthedocs.io/


Development
===========

To run the all tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
