# -*- coding: utf-8 -*-
import dataclasses
import typing as t

import mock
import pytest

from tahoebot.core.application.interactor import (
    Interactor,
    Kwargs,
    RequestModel,
    RequestSchema,
    ResponseModel,
)
from tahoebot.integration.memory import MemoryDao
from tahoebot.core.data.dao import IDao
from tahoebot.core.domain.entity import Entity
from tahoebot.core.domain.repository import (
    IRepository,
    Repository,
    EntitySchema,
)
from tahoebot.exceptions import InvalidRequestError
from tahoebot.utils.dependency_injection import Inject


@dataclasses.dataclass
class Racer(Entity):
    first_name: str
    last_name: str
    age: int
    team: str
    sponsor: str


@dataclasses.dataclass
class Team(Entity):
    name: str
    members: t.List[int]


@dataclasses.dataclass
class Sponsor(Entity):
    name: str


class RegisterRacer(Interactor):
    """Class-based interactor with DI via class fields."""

    racer_repo: Repository = Inject(interface=IRepository, qualifier=Racer)
    team_repo: Repository = Inject(interface=IRepository, qualifier=Team)

    def execute(self, first_name: str,
                last_name: str,
                age: int,
                team: str = None,
                team_id: int = None,
                sponsor: str = 'Nike') -> None:
        if racer_repo.contains(racer_id):
            raise InvalidRequestError
        racer = self.racer_repo.create(racer_id)
        return racer


class LookupRacer(Interactor):
    """Class-based interactor with DI via class fields."""

    racer_repo: Repository = Inject(interface=IRepository, qualifier=Racer)

    def execute(self, racer_id: int) -> None:
        return self.racer_repo.find(racer_id)


@pytest.fixture
def data():
    return {'first_name': 'Jimmy',
            'last_name': 'Quick',
            'age': 25,
            'team': 'Flaming Riots'}


class TestRequestSchema:

    @pytest.fixture
    def schema(self):
        return RequestSchema.from_execute(RegisterRacer.execute)

    def test_init(self, schema):
        assert schema.first_name == {'type': str}
        assert schema.last_name == {'type': str}
        assert schema.age == {'type': int}
        assert schema.team == {'type': str, 'default': None}
        assert schema.team_id == {'type': int, 'default': None}
        assert schema.sponsor == {'type': str, 'default': 'Nike'}

    def test_valid(self, data, schema):
        request = schema.construct(data)
        assert request.first_name == 'Jimmy'
        assert request.last_name == 'Quick'
        assert request.age == 25
        assert request.team == 'Flaming Riots'
        assert request.team_id is None
        assert request.sponsor == 'Nike'

    def test_missing_required(self, data, schema):
        del data['last_name']
        with pytest.raises(InvalidRequestError):
            request = schema.construct(data)

    def test_incorrect_type(self, data, schema):
        data['age'] = 'twenty three'
        with pytest.raises(InvalidRequestError):
            request = schema.construct(data)


class TestSimpleInteractor:

    @pytest.fixture(autouse=True)
    def dao(self, container):
        container.register_by_interface(IDao, MemoryDao, qualifier=Racer,)
        return container.find_by_interface(IDao, qualifier=Racer)

    @pytest.fixture(autouse=True)
    def repo(self, container):
        container.register_by_interface(IRepository,
                                        Repository,
                                        kwargs={'schema': EntitySchema(Racer)},
                                        qualifier=Racer)
        return container.find_by_interface(IRepository, qualifier=Racer)

    @pytest.fixture
    def entity(self, repo):
        data = {'first_name': 'Tim',
                'last_name': 'Jones',
                'age': 22,
                'team': 'Flaming Riots',
                'sponsor': 'Nike'}
        return repo.create_and_add(**data)

    @pytest.fixture
    def interactor(self, container):
        return LookupRacer(container)

    def test_simple_valid_data(self, entity, interactor):
        response = interactor.invoke(racer_id=entity.id)
        assert response.data == entity

    def test_simple_invalid_data(self, entity, interactor):
        response = interactor.invoke(racer_id=entity.id+1)
        assert not response.is_success
